#!/usr/bin/env python
#coding: utf8 
	
import RPi.GPIO as GPIO
import time
# Zählweise der Pins festlegen
GPIO.setmode(GPIO.BOARD)

# Pin 18 (GPIO 24) als Eingang festlegen; Welcher Pins inizialisiern
GPIO.setup(16, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(7, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

# PIN ausgänge
GPIO.setup(38, GPIO.OUT)
GPIO.output(38, GPIO.HIGH)
# Schleifenzähler
bolt = 3
sliding_contact= 3
#Zählt die ausgegeben Zeilen
line=0
zaehler=0
# Dauerschleife, eine Überprüfung soll immer stattfinden.
while 1:
# Prüfen ob der Bolzen ausgefahren oder eingefahren ist. 
# Falls ja ausgefahren bolt = 0, sonst bolt = 1(Farbe von Kabel = Braun zu Grau)
	if GPIO.input(16) == GPIO.HIGH:
		bolt=0
	else:
		bolt=1
		
# Prüfen ob die Tür einen Kontakt zum Schleifkontakt hat. 
# (Bedeutet nur, dass die Türe zu ist, jedoch nicht das der Bolzen ausgefahren ist)
# Falls ein Schleifkontakt vorhanden sliding_contact = 0 sonst sliding_contact = 1 
	if GPIO.input(7) == GPIO.HIGH:
		sliding_contact=1
	else:
		sliding_contact=0

# Prüfen ob ein Inkonsistenter Zustand vorliegt
# bolt==0 und sliding_contact==1 bedeuten, der Bolzen ist ausgefahren, aber die Türe offen (kein Schleifkontakt vorhanden)
	if sliding_contact==1 and bolt==0:
		zaehler=zaehler+1 #Wackelkontakt ausgleichen, zur Sicherheit, erst eingreifen falls der Zustand zweimal hintereinander eintritt
		if zaehler==2: #Das Tür nicht durch Wackeln entriegelt 
		# Da ein Inkonsistenter Zustand vorhanden ist, wird ein Workaround benutzt:
		# Das Relai	wird geschaltet und die Stromversorgung kurz unterbrochen (Rechts und Mitte bei Relai um Kontakte zu unterbrechen). 	
		# Somit wird das Türschloss neu gestartet und der Bolzen fährt rein. --> Zustand wieder wie gewünscht
			GPIO.output(38, GPIO.LOW)
			time.sleep(0.2)
			GPIO.output(38, GPIO.HIGH)
			# Der Zähler wird wieder auf 0 gesetzt
			zaehler=0		
	line=line+1
	print "Grau + Braun (Tür auf)" + str(bolt) + " Tür Status offen "+ str(z)
	print str(line) +" ----------------------"
	time.sleep(1)