import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM) # GPIO Nummern statt Board Nummern


#Pin wird gesetzt
RELAIS_1_GPIO = 17
GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Modus zuweisen

# Kontakt herstellen, Bolzen wird eingezogen, Türe ist auf
GPIO.output(RELAIS_1_GPIO, GPIO.LOW)
time.sleep(0.2)
# Kontakt weg
GPIO.output(RELAIS_1_GPIO, GPIO.HIGH)

GPIO.cleanup()
