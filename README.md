# HCE Android PKCS#15 Smartcard Emulation

The HCE Android PKCS#15 Smartcard Emulation allows the emulation of a contact-less smart card.
It is build to use the SimpleCardAuth-Project (https://github.com/darconeous/SimpleCardAuth) with an Andorid Application instead of an Smartcard.

This App is based on the project of "Android Smart Card Emulator" (https://github.com/frankmorgner/vsmartcard/tree/master/ACardEmulator)
and the "ISO Applet" (https://github.com/philipWendland/IsoApplet).

