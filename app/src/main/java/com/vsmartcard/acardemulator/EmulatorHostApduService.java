/*
 * Copyright (C) 2015 Frank Morgner
 *
 * This file is part of ACardEmulator.
 *
 * ACardEmulator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ACardEmulator is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ACardEmulator.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vsmartcard.acardemulator;

import android.content.Intent;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.vsmartcard.acardemulator.emulators.EmulatorSingleton;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class EmulatorHostApduService extends HostApduService {

    private static String TAG = "NFC_DOOR";
    private static String DIRECTORY_NAME = "pkcs1";
    private int i = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        EmulatorSingleton.createEmulator(this);
        //Read already stored APDUs
        readObjectsPersistent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public byte[] processCommandApdu(byte[] capdu, Bundle extras) {
        //Process the APDU command
        byte[] rapdu = EmulatorSingleton.process(this, capdu);
        //Write the APDUs persistent
        writeObjectsPersistent(capdu, rapdu);
        Log.d(TAG, "finished APDU processing");
        return rapdu;
    }

    /**
     * Send the APDU to a Virtual Smartcard
     *
     * @param readData
     */
    private void sendApduToVSmartcard(byte[] readData) {
        processCommandApdu(readData, null);
    }

    /**
     * Write the Objects PKCS#15 file sturcture, certificate and private Key persistent.
     *
     * @param capdu
     * @param rapdu
     */
    private void writeObjectsPersistent(byte[] capdu, byte[] rapdu) {
        //add an init fuction with init button
        //Create a new file to store the APDUs
        writeByteArray2Disk(DIRECTORY_NAME, "obj" + i++, capdu);

        //Create Object persistent
        if (capdu[1] == (byte) 0x5A && getSwAsInt(rapdu) == 0x9000) {
            Log.d(TAG, "Create Object persistent");
            createObjectPersistent(capdu);
        }
        //Write object persistent
        if (capdu[1] == (byte) 0x54 && getSwAsInt(rapdu) == 0x9000) {
            Log.d(TAG, "Write object persistent");
            writeObjectPersistent(capdu);
        }
        //Write PIN persistent on create
        if (capdu[1] == (byte) 0x40 && getSwAsInt(rapdu) == 0x9000) {
            Log.d(TAG, "Write PIN persistent on create");
            writePinPersistentOnCreate(capdu);
        }
        //Write PIN persistent on change
        if (capdu[1] == (byte) 0x44 && getSwAsInt(rapdu) == 0x9000) {
            Log.d(TAG, "Write PIN persistent on change");
            writePinPersistentOnChange(capdu);
        }
    }

    @Override
    public void onDeactivated(int reason) {
        Intent i = new Intent(EmulatorSingleton.TAG);
        Log.d("", "End transaction");
        switch (reason) {
            case DEACTIVATION_LINK_LOSS:
                i.putExtra(EmulatorSingleton.EXTRA_DESELECT, "link lost");
                EmulatorSingleton.deactivate();
                break;
            case DEACTIVATION_DESELECTED:
                EmulatorSingleton.deactivate();
                break;
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(i);
    }

    /**
     * Get Byte Array as int
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param responseApdu
     * @return
     */
    private static int getSwAsInt(byte[] responseApdu) {
        byte[] byteArray = new byte[4];
        byteArray[0] = 0;
        byteArray[1] = 0;
        System.arraycopy(Arrays.copyOfRange(responseApdu, responseApdu.length - 2, responseApdu.length),
                0, byteArray, 2, 2);
        return byteArray2int(byteArray);
    }

    /**
     * Convert Byte to int
     * Code from http://stackoverflow.com/questions/7619058/convert-a-byte-array-to-integer-in-java-and-vise-versa
     *
     * @param byteArray
     * @return
     */
    private static int byteArray2int(byte[] byteArray) {
        ByteBuffer wrapped = ByteBuffer.wrap(byteArray);
        return wrapped.getInt();
    }

    /**
     * Create a persistent Object
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param createObjectApdu
     */
    private void createObjectPersistent(byte[] createObjectApdu) {
        byte[] writeableCreateObjectApdu = new byte[createObjectApdu.length - 1];
        System.arraycopy(createObjectApdu, 0, writeableCreateObjectApdu, 0, 4);
        System.arraycopy(createObjectApdu, 5, writeableCreateObjectApdu, 4, createObjectApdu.length - 5);
        writeByteArray2Disk("objects", printByteArray(Arrays.copyOfRange(createObjectApdu, 5, 9)) + "_c", writeableCreateObjectApdu);
    }

    /**
     * Write a object persistent
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param writeObjectApdu
     */
    private void writeObjectPersistent(byte[] writeObjectApdu) {
        byte[] objectID = Arrays.copyOfRange(writeObjectApdu, 5, 9);
        byte[] offset = Arrays.copyOfRange(writeObjectApdu, 9, 13);
        byte[] data = Arrays.copyOfRange(writeObjectApdu, 14, writeObjectApdu.length);
        byte[] readData = readByteArrayFromDisk("objects", printByteArray(objectID));

        if (readData != null) {
            //There is already data in the file
            int length = Math.max(readData.length, byteArray2int(offset) + data.length);
            byte[] newData = new byte[length];
            System.arraycopy(readData, 0, newData, 0, readData.length);
            System.arraycopy(data, 0, newData, byteArray2int(offset), data.length);
            writeByteArray2Disk("objects", printByteArray(objectID), newData);
        } else {
            //File has been empty
            byte[] newData = new byte[byteArray2int(offset) + data.length];
            System.arraycopy(data, 0, newData, byteArray2int(offset), data.length);
            writeByteArray2Disk("objects", printByteArray(objectID), newData);
        }
    }


    /**
     * Write the Pin persistent, if the Pin is created new
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param createPinApdu
     */
    private void writePinPersistentOnCreate(byte[] createPinApdu) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //Write unsuccessful tries and unsuccessful unblock tries first
        out.write((byte) 0);
        out.write((byte) 0);
        try {
            out.write(createPinApdu);
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeByteArray2Disk("pins", "pin" + createPinApdu[2], out.toByteArray());
    }

    /**
     * Write the Pin persistent, if the existing Pin is changed
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param changePinApdu
     */
    private void writePinPersistentOnChange(byte[] changePinApdu) {
        byte[] readData = readByteArrayFromDisk("pins", "pin"
                + changePinApdu[2]);
        byte[] createPinApdu = Arrays.copyOfRange(readData, 2, readData.length);
        try {
            ByteArrayOutputStream outputStreamChanged = new ByteArrayOutputStream();
            //Write unsuccessfultries and unsuccessfulunblocktries first
            outputStreamChanged.write(readData[0]);
            outputStreamChanged.write(readData[1]);
            outputStreamChanged.write(Arrays.copyOfRange(createPinApdu, 0, 4));
            outputStreamChanged.write(Arrays.copyOfRange(changePinApdu,
                    changePinApdu[5] + 6, changePinApdu.length));
            outputStreamChanged.write(Arrays.copyOfRange(createPinApdu,
                    createPinApdu[5] + 6, createPinApdu.length));
            //Set LC-Byte
            byte[] byteArrayWithoutLc = outputStreamChanged.toByteArray();
            byte lc = (byte) (byteArrayWithoutLc.length - 2 - 4);
            byte[] byteArrayWithLc = new byte[2 + 4 + 1 + lc];
            System.arraycopy(byteArrayWithoutLc, 0, byteArrayWithLc, 0, 6);
            byteArrayWithLc[6] = lc;
            System.arraycopy(byteArrayWithoutLc, 6, byteArrayWithLc, 7, lc);
            writeByteArray2Disk("pins", "pin" + changePinApdu[2],
                    byteArrayWithLc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writing data to persistent memory
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param directoryName
     * @param fileName
     * @param data
     */
    private void writeByteArray2Disk(String directoryName, String fileName, byte[] data) {
        File directory = createDirectory(directoryName);
        try {
            File file = new File(directory, fileName);
            FileOutputStream fos = new FileOutputStream(file);
            DataOutputStream dos = new DataOutputStream(fos);
            dos.write(data);
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a Directory
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param name
     * @return
     */
    private File createDirectory(String name) {
        File directory = new File(this.getFilesDir(),
                name);
        if (!directory.exists()) {
            directory.mkdir();
            directory.setReadable(true, true);
            directory.setWritable(true, true);
            directory.setExecutable(true, true);
        }
        return directory;
    }

    /**
     * Small helping function for debugging
     * Code copied from http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
     */
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String printByteArray(byte[] byteArray) {
        char[] hexChars = new char[byteArray.length * 2];
        for (int i = 0; i < byteArray.length; i++) {
            int v = byteArray[i] & 0xFF;
            hexChars[i * 2] = hexArray[v >>> 4];
            hexChars[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Reading data from persistent memory
     * Code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     *
     * @param directoryName
     * @param fileName
     * @return
     */
    private byte[] readByteArrayFromDisk(String directoryName, String fileName) {
        File directory = new File(this.getFilesDir(),
                directoryName);
        if (directory.exists()) {
            File file = new File(directory, fileName);
            if (file.exists()) {
                FileInputStream fis;
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                try {
                    fis = new FileInputStream(file);
                    // Read data
                    int readBytes;
                    do {
                        byte[] buffer = new byte[100];
                        readBytes = fis.read(buffer);
                        if (readBytes > 0) {
                            outputStream.write(Arrays.copyOfRange(buffer, 0,
                                    readBytes));
                        }
                    } while (readBytes == 100);
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return outputStream.toByteArray();
            }
        }
        return null;
    }

    //NILS

    /**
     * Read persistent Objects
     * Modified code from https://github.com/eriknellessen/Virtual-Keycard/blob/dcdab643d2ba2c10bbe8bcba42608f19f2f0c121/android-projects/Muscle_Card_on_Android/src/de/nellessen/muscle_card_on_android/MSCHostApduService.java
     */
    private void readObjectsPersistent() {
        byte[] header = {(byte) 0xB0, (byte) 0x54, (byte) 0x00, (byte) 0x00};
        File objectsDirectory = createDirectory(DIRECTORY_NAME);
        File[] files = objectsDirectory.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                byte[] readData = readByteArrayFromDisk(DIRECTORY_NAME, fileName);
                if (readData != null) {
                    sendApduToVSmartcard(readData);
                }
            }
        }
    }

    /**
     * Converts a hex string to a byte Array
     *
     * @param str
     * @return
     */
    private static byte[] string2byteArray(String str) {
        if (str.length() % 2 != 0) {
            System.out.println("string2byteArray got string with wrong length!");
            return null;
        }
        byte[] returnValue = new byte[str.length() / 2];
        for (int i = 0; i < str.length(); i++) {
            byte value;
            if (str.charAt(i) < 58) {
                value = (byte) (str.charAt(i) - 48);
            } else {
                value = (byte) (str.charAt(i) - 55);
            }
            returnValue[i / 2] += (byte) (value << (4 * ((i + 1) % 2)));
        }
        return returnValue;
    }

    /**
     * Convert int to byte Array
     * Code from http://stackoverflow.com/questions/7619058/convert-a-byte-array-to-integer-in-java-and-vise-versa
     *
     * @param givenInt
     * @return
     */
    private static byte[] int2byteArray(int givenInt) {
        ByteBuffer dbuf = ByteBuffer.allocate(4);
        dbuf.putInt(givenInt);
        return dbuf.array();
    }

}
