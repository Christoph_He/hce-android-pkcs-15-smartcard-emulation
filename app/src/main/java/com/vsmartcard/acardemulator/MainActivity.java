/*
 * Copyright (C) 2015 Frank Morgner
 *
 * This file is part of ACardEmulator.
 *
 * ACardEmulator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ACardEmulator is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ACardEmulator.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vsmartcard.acardemulator;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.vsmartcard.acardemulator.emulators.EmulatorSingleton;

public class MainActivity extends AppCompatActivity {

    private TextView textViewVPCDStatus;
    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(EmulatorSingleton.TAG)) {
                String capdu = intent.getStringExtra(EmulatorSingleton.EXTRA_CAPDU);
                String rapdu = intent.getStringExtra(EmulatorSingleton.EXTRA_RAPDU);
                String error = intent.getStringExtra(EmulatorSingleton.EXTRA_ERROR);
                String deselect = intent.getStringExtra(EmulatorSingleton.EXTRA_DESELECT);
                String install = intent.getStringExtra(EmulatorSingleton.EXTRA_INSTALL);
                if (install != null)
                    textViewVPCDStatus.append(getResources().getString(R.string.status_install) + ":" + install + "\n");
                if (capdu != null)
                    textViewVPCDStatus.append(getResources().getString(R.string.status_capdu) + ": " + capdu + "\n");
                if (error != null)
                    textViewVPCDStatus.append(getResources().getString(R.string.status_error) + ": " + error + "\n");
                if (rapdu != null) {
                    textViewVPCDStatus.append(getResources().getString(R.string.status_rapdu) + ": " + rapdu + "\n");
                }
                if (deselect != null)
                    textViewVPCDStatus.append(getResources().getString(R.string.status_disconnected) + ": " + deselect + "\n");
            }
        }
    };
    private AlertDialog dialog;
    final String PREFS = "ACardEmulatorPrefs";
    final String PREF_LASTVERSION = "last version";
    private static String SAVED_STATUS = "textViewVPCDStatus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewVPCDStatus = (TextView) findViewById(R.id.textViewLog);
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(EmulatorSingleton.TAG);
        bManager.registerReceiver(bReceiver, intentFilter);

        Intent serviceIntent = new Intent(this.getApplicationContext(), SmartcardProviderService.class);
        startService(serviceIntent);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textViewVPCDStatus.setText(savedInstanceState.getCharSequence(SAVED_STATUS));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putCharSequence(SAVED_STATUS, textViewVPCDStatus.getText());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause() {
        if (dialog != null)
            dialog.dismiss();
        super.onPause();
    }
}
