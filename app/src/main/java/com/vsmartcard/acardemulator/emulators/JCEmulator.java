package com.vsmartcard.acardemulator.emulators;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.licel.jcardsim.base.Simulator;
import com.licel.jcardsim.base.SimulatorRuntime;
import com.licel.jcardsim.utils.AIDUtil;
import com.vsmartcard.acardemulator.R;

import net.pwendland.javacard.pki.isoapplet.IsoApplet;

public class JCEmulator implements Emulator {

    private static Simulator simulator = null;

    public JCEmulator(Context context) {
        String aid, name, extra_install = "", extra_error = "";
        simulator = new Simulator(new SimulatorRuntime());

        name = context.getResources().getString(R.string.applet_isoapplet);
        aid = context.getResources().getString(R.string.aid_isoapplet);
        try {
            simulator.installApplet(AIDUtil.create(aid), IsoApplet.class);
            extra_install += "\n" + name + " (AID: " + aid + ")";
        } catch (Exception e) {
            e.printStackTrace();
            extra_error += "\n" + "Could not install " + name + " (AID: " + aid + ")";
        }

        Intent i = new Intent(EmulatorSingleton.TAG);
        if (!extra_error.isEmpty())
            i.putExtra(EmulatorSingleton.EXTRA_ERROR, extra_error);
        if (!extra_install.isEmpty())
            i.putExtra(EmulatorSingleton.EXTRA_INSTALL, extra_install);
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }

    public void destroy() {
        if (simulator != null) {
            simulator.reset();
            simulator = null;
        }
    }

    public byte[] process(byte[] commandAPDU) {
        return simulator.transmitCommand(commandAPDU);
    }

    public void deactivate() {
        simulator.reset();
    }
}